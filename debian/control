Source: groovy-emacs-modes
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Sławomir Wójcik <valdaer@gmail.com>
Build-Depends: debhelper-compat (= 13),
               dh-elpa,
               elpa-dash,
               elpa-s,
               elpa-f,
               elpa-shut-up
Standards-Version: 4.5.0
Homepage: https://github.com/Groovy-Emacs-Modes/groovy-emacs-modes
Vcs-Browser: https://salsa.debian.org/emacsen-team/groovy-emacs-modes
Vcs-Git: https://salsa.debian.org/emacsen-team/groovy-emacs-modes.git
Testsuite: autopkgtest-pkg-elpa

Package: elpa-groovy-mode
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: Major mode for Groovy source files
 This package contains groovy major mode providing syntax highlighting and
 editing for Groovy and Gradle source files. Additionally it provides REPL like
 capabilities by running groovy process in a buffer(inferior mode).


Package: elpa-grails-mode
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends},
         elpa-groovy-mode
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: Minor mode for Grails framework source files
 A minor mode that adds some useful commands for navigating around
 a grails project
 .
 In the root of the grails project (where the grails-app directory is)
 add this to your .dir-locals.el file (v23+)
 (groovy-mode . ((grails-mode . 1)))
 (java-mode . ((grails-mode . 1)))
 (html-mode . ((grails-mode . 1)))
 .
 This will turn on grails minor mode whenever a groovy, java or gsp file is
 opened, this presumes you have gsp files et to use html-mode adjust to whatever
 mode gsp files use
 .
 or just add this to have grails mode with any file in that directory structure
 .
 ((nil . ((grails-mode . 1))))
 .
 The main addition is a view in anything that shows all the grails project files
